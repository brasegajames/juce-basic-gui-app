# Juce Basic GUI App

This project is a simple GUI application that allows you to create a simple GUI and export it through a file.


## Getting Started

To start the project you will need Juce and a compiler like Visual Studio, XCode, or CodeBlocks.
You can also download the .exe file in the "Releases" folder in the repo to experiment with the application without compiling it.

### Using the .jucer file included

You can clone this project and launch the "GUI Basic App.jucer" to open Projucer. 
Then you can open the IDE of your choice to compile it.

### Starting from a new project

You also could create a new project in Projucer, add the sources from the repo into the "Source" folder. 
Then you can open the IDE of your choice to compile it.

## Features

Once the application launched, you can interact with it.

### Buttons

The button "+" allows you to create root components. You can drag them through the white canvas. You also can resize them.
The button "Export" allows you to export the components through a file called "export.txt". This file could look like this :

```
MasterComponent w="869" h="477"
## Component 1 color="0x969696" x="15" y="333" w="839" h="136"
#### Component 1.1 color="0x06636A" x="68" y="8" w="754" h="119"
#### Component 1.2 color="0x00A7FB" x="15" y="6" w="39" h="121"
## Component 2 color="0x4181A1" x="9" y="6" w="319" h="316"
#### Component 2.1 color="0x0000FF" x="76" y="17" w="232" h="156"
#### Component 2.2 color="0x0075AF" x="13" y="182" w="294" h="123"
#### Component 2.3 color="0x18313E" x="8" y="12" w="59" h="163"
###### Component 2.3.1 color="0x382626" x="13" y="15" w="30" h="30"
###### Component 2.3.2 color="0x382626" x="14" y="66" w="30" h="30"
###### Component 2.3.3 color="0x382626" x="15" y="115" w="30" h="30"
## Component 3 color="0x4A829E" x="344" y="7" w="512" h="315"
#### Component 3.1 color="0x382726" x="5" y="5" w="247" h="303"
###### Component 3.1.1 color="0x346075" x="8" y="12" w="229" h="194"
###### Component 3.1.2 color="0x14394C" x="10" y="208" w="227" h="79"
#### Component 3.2 color="0x382626" x="254" y="4" w="252" h="304"
###### Component 3.2.1 color="0x385B6C" x="13" y="11" w="227" h="197"
###### Component 3.2.2 color="0x213037" x="14" y="210" w="226" h="78"
```

You can see the result of this file [here](https://i.imgur.com/CAz4WTr.png)

### Components interactions

A component can be selected by putting the mouse inside it. (The border of the component grows and turns red)
You can move a component by clicking on it and by dragging it, and resize it by dragging the bottom right corner.
You also can change the color of a component by double clicking on it and picking the color you want.

### Keyboard Interactions

You can add childs to a root component by pressing the "spacebar" key while the component is selected.
You also can delete a component and all its childs by pressing the "backspace" or the "delete"/"suppr" key while the component is selected.
