/*
  ==============================================================================

    MasterComponent.h
    Created: 13 Feb 2018 3:59:47am
    Author:  Brase-Prod

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class MasterComponent    : public Component
{
public:
    MasterComponent();
    ~MasterComponent();

    void paint (Graphics&) override;
    void resized() override;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MasterComponent)
};
