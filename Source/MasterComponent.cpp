/*
  ==============================================================================

    MasterComponent.cpp
    Created: 13 Feb 2018 3:59:47am
    Author:  Brase-Prod

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MasterComponent.h"

//==============================================================================
MasterComponent::MasterComponent()
{
}

MasterComponent::~MasterComponent()
{
}

void MasterComponent::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::white);
	g.fillAll();
}

void MasterComponent::resized()
{
}
