/*
  ==============================================================================

    DraggableComponent.h
    Created: 13 Feb 2018 3:17:51am
    Author:  Brase-Prod

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class DraggableComponent : public Component, public ChangeListener
{
public:
	DraggableComponent();
	~DraggableComponent();

	void paint(Graphics&) override;
	void resized() override;
	
	void mouseDown(const MouseEvent& e) override;
	void mouseDrag(const MouseEvent& e) override;
	void mouseEnter(const MouseEvent& e) override;
	void mouseExit(const MouseEvent& e) override;
	void mouseDoubleClick(const MouseEvent& e) override; 
	void changeListenerCallback(ChangeBroadcaster* source) override;
	
	// Creates a child if the mouse is inside a component. else searches for childs
	bool addChild(); 

	// Deletes recursively a component if the mouse is inside. else searches for childs
	bool deleteChild();

	// Returns the string of a component.
	String getString(); 

	// Returns the string of the component and its childs
	String getChildsString();

	bool getIsMouseIn() { return isMouseIn; }
	
private:
	ComponentBoundsConstrainer constrainer;
	ScopedPointer<ResizableCornerComponent> resizer = nullptr;
	ComponentDragger dragger;
	bool isMouseIn = false;
	Colour colour = Colours::blue;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DraggableComponent)
};