/*
  ==============================================================================

    DraggableComponent.cpp
    Created: 13 Feb 2018 3:17:51am
    Author:  Brase-Prod

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "DraggableComponent.h"
#include "MasterComponent.h"

//==============================================================================
DraggableComponent::DraggableComponent()
{
	this->addChildComponent(resizer = new ResizableCornerComponent(this, &constrainer));
}

DraggableComponent::~DraggableComponent()
{
	for each (Component* var in getChildren())
	{
		if (dynamic_cast<DraggableComponent*>(var) != nullptr)
			delete(var);
	}
}

void DraggableComponent::paint (Graphics& g)
{
	Rectangle<float> area(getLocalBounds().toFloat().reduced(2.0f));

	ColourSelector cs;
	g.setColour(colour);
	g.fillRect(area);

	g.setColour(Colours::black);
	isMouseOver(false) ? g.setColour(Colours::red), g.drawRect(area, 2.5f) : g.drawRect(area, 1.0f);
}

void DraggableComponent::resized()
{
	constrainer.setMinimumOnscreenAmounts(getHeight(), getWidth(), getHeight(), getWidth());
	constrainer.setMinimumHeight(30);
	constrainer.setMinimumWidth(30);

	if (resizer != nullptr)
	{
		resizer->setVisible(true);
		const int resizerSize = 12;
		resizer->setBounds(getWidth() - resizerSize,
			getHeight() - resizerSize,
			resizerSize, resizerSize);
	}
}

void DraggableComponent::mouseDown(const MouseEvent& e)
{
	dragger.startDraggingComponent(this, e);
}

void DraggableComponent::mouseDrag(const MouseEvent& e)
{
	dragger.dragComponent(this, e, &constrainer);
}

void DraggableComponent::mouseEnter(const MouseEvent&)
{
	repaint();
}

void DraggableComponent::mouseExit(const MouseEvent&)
{
	repaint();
}


void DraggableComponent::mouseDoubleClick(const MouseEvent&)
{
	ColourSelector* colourSelector = new ColourSelector();
	colourSelector->setName("background");
	colourSelector->setCurrentColour(findColour(TextButton::buttonColourId));
	colourSelector->addChangeListener(this);
	colourSelector->setColour(ColourSelector::backgroundColourId, Colours::transparentBlack);
	colourSelector->setSize(300, 400);

	CallOutBox::launchAsynchronously(colourSelector, getScreenBounds(), nullptr);
}

void DraggableComponent::changeListenerCallback(ChangeBroadcaster* source)
{
	if (ColourSelector* cs = dynamic_cast<ColourSelector*> (source))
		colour = cs->getCurrentColour();
	repaint();
}

bool DraggableComponent::addChild()
{
	if (isMouseOver(true))
	{
		if (isMouseOver(false))
		{
			DraggableComponent* comp = new DraggableComponent();
			addChildAndSetID(comp, this->getComponentID() + "." + String(this->getNumChildComponents()));

			comp->centreWithSize(80, 50);
			return true;
		}
		else
		{
			for each (Component* var in getChildren())
			{
				if (dynamic_cast<DraggableComponent*>(var) != nullptr)
					dynamic_cast<DraggableComponent*>(var)->addChild();
			}
		}
	}
	return false;
}

bool DraggableComponent::deleteChild()
{
	if (isMouseOver(true))
	{
		if (isMouseOver(false))
		{
			delete(this);
			return true;
		}
		else
		{
			for each (Component* var in getChildren())
			{
				if (dynamic_cast<DraggableComponent*>(var) != nullptr)
				{
					dynamic_cast<DraggableComponent*>(var)->deleteChild();
				}
			}
		}
	}
	return false;
}

String DraggableComponent::getString()
{
	String res = "";
	Component* tmp = this->getParentComponent();
	res += "##";

	while (dynamic_cast<MasterComponent*>(tmp) == nullptr)
	{
		tmp = tmp->getParentComponent();
		res += "##";
	}

	res += " Component " + getComponentID() 
		+ " color=\"0x" +  colour.toDisplayString(false) + '"'
		+ " x=\"" + String(getX()) + '"'
		+ " y=\"" + String(getY()) + '"'
		+ " w=\"" + String(getWidth()) + '"'
		+ " h=\"" + String(getHeight()) + '"' + "\n";
	return res;
}

String DraggableComponent::getChildsString()
{
	String res = "";
	res += getString();

	if (getNumChildComponents() > 0)
	{
		for each (Component* var in getChildren())
		{
			if (dynamic_cast<DraggableComponent*>(var) != nullptr)
			{
				res += dynamic_cast<DraggableComponent*>(var)->getChildsString();
			}
		}
	}
	return res;
}