/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent()
{
	addAndMakeVisible(addNewComponent);
	addNewComponent.setButtonText("+");
	addNewComponent.addListener(this);

	addAndMakeVisible(exportFile);
	exportFile.setButtonText("Export");
	exportFile.addListener(this);

	setWantsKeyboardFocus(true);

	addAndMakeVisible(masterComponent);

    setSize (600, 400);
}

MainContentComponent::~MainContentComponent()
{
	addNewComponent.removeListener(this);

	masterComponent.deleteAllChildren();
}

void MainContentComponent::resized()
{
	addNewComponent.setBounds(10, 10, getWidth() / 2 - 20, 40);
	exportFile.setBounds(getWidth() / 2 + 10, 10, getWidth() / 2 - 20, 40);

	masterComponent.setBounds(10, 60, getWidth() - 20, getHeight() - 70);
}


void MainContentComponent::exportComponentsToFile()
{
	std::ofstream myfile;
	myfile.open("export.txt");

	myfile << "MasterComponent w=\"" << masterComponent.getWidth() << "\" h=\"" << masterComponent.getHeight() << "\"\n";

	for each (Component* var in masterComponent.getChildren())
	{
		if (dynamic_cast<DraggableComponent*>(var) != nullptr)
		{
			myfile << dynamic_cast<DraggableComponent*>(var)->getChildsString();
		}
	}
	myfile.close();
}

void MainContentComponent::buttonClicked(Button* button) 
{
	if (button == &addNewComponent)
	{
		DraggableComponent* comp = new DraggableComponent();
		addAndMakeVisible(comp);

		masterComponent.addChildAndSetID(comp, String(masterComponent.getNumChildComponents() + 1));
		comp->centreWithSize(80, 50);
	}
	else if (button == &exportFile)
	{
		exportComponentsToFile();
	}
}

bool MainContentComponent::keyPressed(const KeyPress &k)
{
	if (k.getKeyCode() == KeyPress::spaceKey)
	{
		for each (DraggableComponent* var in masterComponent.getChildren())
		{
			if (dynamic_cast<DraggableComponent*>(var) != nullptr)
				var->addChild();
		}
	}
	if (k.getKeyCode() == KeyPress::deleteKey || k.getKeyCode() == KeyPress::backspaceKey)
	{
		for each (DraggableComponent* var in masterComponent.getChildren())
		{
			if (dynamic_cast<DraggableComponent*>(var) != nullptr)
				var->deleteChild();
		}
	}
	return false;
}