/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "DraggableComponent.h"
#include "MasterComponent.h"
#include <iostream>
#include <fstream>


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public Component, private Button::Listener
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void resized() override;
	void buttonClicked(Button* button) override;
	bool keyPressed(const KeyPress &k) override;

	// Creates a file and fill it with the components informations
	void exportComponentsToFile();

private:
	TextButton addNewComponent;
	TextButton exportFile;

	MasterComponent masterComponent;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
